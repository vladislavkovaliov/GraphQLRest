const graphql = require('graphql');

module.exports = UserType = new graphql.GraphQLObjectType({
  name: 'Profile',
  fields: () => ({
    _id: { type: graphql.GraphQLString },
    firebaseId: { type: graphql.GraphQLString },
    index: { type: graphql.GraphQLInt },
    email: { type: graphql.GraphQLString },
    login: { type: graphql.GraphQLString },
    name: { type: graphql.GraphQLString },
    password: { type: graphql.GraphQLString },
    created: { type: graphql.GraphQLString },
    gender: { type: graphql.GraphQLString },
    age: { type: graphql.GraphQLInt },
    hireDate: { type: graphql.GraphQLString },
    dateOfBirth: { type: graphql.GraphQLString },
    lastUpdated: { type: graphql.GraphQLString },
    guid: { type: graphql.GraphQLString },
    picture: { type: graphql.GraphQLString },
    department: { type: graphql.GraphQLString },
    homeAddress: { type: graphql.GraphQLString },
    phones: { type: new graphql.GraphQLList(graphql.GraphQLString) },
    stories: { type: new graphql.GraphQLList(graphql.GraphQLString) },
    followers: { type: new graphql.GraphQLList(graphql.GraphQLString) },
    following: { type: new graphql.GraphQLList(graphql.GraphQLString) },
    role: { type: graphql.GraphQLString },
  })
});
