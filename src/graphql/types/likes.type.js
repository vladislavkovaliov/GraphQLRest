const graphql = require('graphql');

module.exports = LikesType = new graphql.GraphQLObjectType({
  name: 'Likes',
  description: 'Adding firebase id into likes of story by story id',
  fields: () => ({
    _id: { type: graphql.GraphQLString },
    firebaseId: { type: graphql.GraphQLString },
  })
});
