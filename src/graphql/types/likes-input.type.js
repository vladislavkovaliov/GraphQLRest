const graphql = require('graphql');

module.exports = LikesInputType = new graphql.GraphQLInputObjectType({
  name: 'LikesInput',
  description: 'Removing firebase id into likes of story by story id',
  fields: () => ({
    _id: { type: graphql.GraphQLString },
    firebaseId: { type: graphql.GraphQLString },
  })
});
