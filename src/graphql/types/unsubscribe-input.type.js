const graphql = require('graphql');

module.exports = SubscribeInputType = new graphql.GraphQLInputObjectType({
  name: 'UnsubscribeInput',
  description: 'Unsubscribe payload definition',
  fields: () => ({
    firebaseId: { type: new graphql.GraphQLList(graphql.GraphQLString) },
    firebaseIdToUnsubscribe: { type: new graphql.GraphQLList(graphql.GraphQLString) },
  })
});
