const UserType = require('./user.type');
const UserInputType = require('./user-input.type');
const ProfileType = require('./profile.type');
const ProfileInputType = require('./profile-input.type');
const StoryType = require('./story.type');
const StoryInputType = require('./story-input.type');
const ImageType = require('./image.type');
const ImageInputType = require('./image-input.type');
const SubscribeInputType = require('./subscribe-input.type');
const UnsubscribeInputType = require('./unsubscribe-input.type');
const LikesType = require('./likes.type');
const LikesInputType = require('./likes-input.type');

module.exports = {
  UserType,
  UserInputType,

  ProfileType,
  ProfileInputType,

  StoryType,
  StoryInputType,

  ImageType,
  ImageInputType,

  SubscribeInputType,
  UnsubscribeInputType,

  LikesType,
  LikesInputType,
};
