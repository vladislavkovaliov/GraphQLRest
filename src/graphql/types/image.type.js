const graphql = require('graphql');

module.exports = ImageType = new graphql.GraphQLObjectType({
  name: 'Image',
  fields: () => ({
    _id: { type: graphql.GraphQLString },
    guid: { type: graphql.GraphQLString },
    firebaseId: { type: graphql.GraphQLString },
    url: { type: graphql.GraphQLString },
    profileId: { type: graphql.GraphQLString },
  })
});
