const graphql = require('graphql');

module.exports = ImageInputType = new graphql.GraphQLInputObjectType({
  name: 'ImageInput',
  fields: () => ({
    _id: { type: graphql.GraphQLString },
    guid: { type: graphql.GraphQLString },
    firebaseId: { type: graphql.GraphQLString },
    url: { type: graphql.GraphQLString },
    profileId: { type: graphql.GraphQLString },
  })
});
