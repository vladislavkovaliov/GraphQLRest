const graphql = require('graphql');

module.exports = SubscribeInputType = new graphql.GraphQLInputObjectType({
  name: 'SubscribeInput',
  description: 'Subscribe payload definition',
  fields: () => ({
    firebaseId: { type: graphql.GraphQLString },
    onFirebaseId: { type: graphql.GraphQLString },
  })
});
