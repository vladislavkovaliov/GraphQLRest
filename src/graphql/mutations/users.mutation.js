const graphql = require('graphql');
const profiles = require('../../data/profiles.json');
const { UserType, StoryType, ProfileType, UserInputType, ProfileInputType, StoryInputType } = require('../types');
const { find } = require('lodash');
const { Users } = require('../../models/users.model');

module.exports = {
  updateUser: {
    type: UserType,
    args: {
      input: {
        type: new graphql.GraphQLNonNull(UserInputType),
      },
    },
    resolve: async (rootValue, { input }) => {
      const query = {
        id: input.id
      };
      const options = {
        new: true
      };

      return await Users.findOneAndUpdate(query, input, options);
    },
  },
};
