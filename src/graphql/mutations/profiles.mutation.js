const graphql = require('graphql');
const { ProfileType, ProfileInputType, SubscribeInputType, UnsubscribeInputType} = require('../types');
const { Profiles } = require('../../models/profiles.model');

module.exports = {
  updateProfile: {
    type: ProfileType,
    args: {
      input: {
        type: new graphql.GraphQLNonNull(ProfileInputType),
      },
    },
    resolve: async (rootValue, { input }) => {
      const query = {
        id: input.id
      };
      const options = {
        new: true
      };

      return await Profiles.findOneAndUpdate(query, input, options);
    },
  },
  insertProfile: {
    type: ProfileType,
    args: {
      input: {
        type: ProfileInputType,
      },
    },
    resolve: async (rootValue, args) => {
      try {
        const profile = await Profiles.create(args.input);

        return profile;
      }
      catch (ex) {
        console.log(ex);
      }
    },
  },
  deleteProfile: {
    type: ProfileType,
    args: {
      _id: {
        type: graphql.GraphQLString,
      },
      guid: {
        type: graphql.GraphQLString,
      },
    },
    resolve: async (rootValue, args) => {
      try {
        if (args._id) {
          const profile = await Profiles.findByIdAndRemove(args._id);

          return profile;
        }

        if (args.guid) {
          const profile = await Profiles.findOneAndRemove({ guid: args.guid });

          return profile;
        }

        return null;
      }
      catch (ex) {
        console.log(ex);
      }
    },
  },
  subscribe: {
    type: ProfileType,
    args: {
      input: {
        type: SubscribeInputType,
      },
    },
    resolve: async (rootValue, args) => {
      try {
        console.log(args);
        const { firebaseId, onFirebaseId } = args.input;
        const profile = await Profiles
          .findOneAndUpdate(
            { firebaseId: onFirebaseId },
            {
              $addToSet: {
                followers: firebaseId,
              },
            },
            { new: true }
          );
        console.log(profile);
        await Profiles
          .findOneAndUpdate(
            { firebaseId },
            {
              $addToSet: {
                following: onFirebaseId,
              }
            },
            { new: true }
          );

        return profile;
      } catch(ex) {
        console.log(ex);
      }
    },
  },
  unsubscribe: {
    type: ProfileType,
    args: {
      input: {
        type: UnsubscribeInputType,
      },
    },
    resolve: async (rootValue, args) => {
      try {
        const { firebaseId, firebaseIdToUnsubscribe } = args.input;
        const profile = await Profiles
          .findOneAndUpdate(
            { firebaseId: firebaseIdToUnsubscribe },
            {
              $pull: {
                followers: firebaseId,
              },
            },
            { new: true }
          );

        await Profiles
          .findOneAndUpdate(
            { firebaseId },
            {
              $pull: {
                following: firebaseIdToUnsubscribe,
              }
            },
            { new: true }
          );

        return profile;
      } catch(ex) {
        console.log(ex);
      }
    },
  },
};
