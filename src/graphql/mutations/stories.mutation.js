const graphql = require('graphql');
const { StoryType, StoryInputType, LikesType, LikesInputType } = require('../types');
const { find } = require('lodash');
const { Stories } = require('../../models/stories.model');
const { deleteProps } = require('../../utils/delete-props.util');

module.exports = {
  insertStory: {
    type: StoryType,
    args: {
      guid: {
        type: graphql.GraphQLString,
      },
      picture: {
        type: graphql.GraphQLString,
      },
      text: {
        type: new graphql.GraphQLNonNull(graphql.GraphQLString),
      },
      date: {
        type: graphql.GraphQLString,
      },
      title: {
        type: graphql.GraphQLString,
      },
      likes: {
        type: new graphql.GraphQLList(graphql.GraphQLString),
      },
      comments: {
        type: new graphql.GraphQLList(graphql.GraphQLString),
      },
      firebaseId: {
        type: graphql.GraphQLString,
      }
    },
    resolve: async (rootValue, input) => {
      console.log(input);
       try {
        const story = await Stories.create(input);

        return story;
      } catch (ex) {
        console.log(ex);
      }
    },
  },
  updateStory: {
    type: StoryType,
    args: {
      _id: {
        type: graphql.GraphQLString,
      },
      text: {
        type: graphql.GraphQLString,
      },
      guid: {
        type: graphql.GraphQLString,
      },
    },
    resolve: async (rootValue, args) => {
      try {
        if (args._id) {
          const story = await Stories.findByIdAndUpdate(args._id, args, { new: true });

          return story;
        }

        if (args.guid) {
          const story = await Stories.findOneAndUpdate({ guid: args.guid }, args, { new: true });

          return story;
        }

        return null;
      }
      catch (ex) {
        console.log(ex);
      }
    },
  },
  deleteStory: {
    type: StoryType,
    args: {
      _id: {
        type: graphql.GraphQLString,
      },
      guid: {
        type: graphql.GraphQLString,
      },
    },
    resolve: async (rootValue, args) => {
      try {
        if (args._id) {
          const story = await Stories.findByIdAndRemove(args._id);

          return story;
        }

        if (args.guid) {
          const story = await Stories.findOneAndRemove({ guid: args.guid });

          return story;
        }

        return null;
      }
      catch (ex) {
        console.log(ex);
      }
    },
  },
  addLike: {
    type: StoryType,
    args: {
      storyId: {
        type: graphql.GraphQLString,
      },
      firebaseId: {
        type: graphql.GraphQLString,
      },
    },
    resolve: async (rootValue, args) => {
      try {
        const { storyId, firebaseId } = args;
        console.log(args);

        const story = await Stories.findByIdAndUpdate(
          { _id: storyId },
          {
            $addToSet: {
              likes: firebaseId,
            },
          },
          { new: true }
        );
        console.log(story);
        return story;
      } catch (ex) {
        console.log(ex);
      }
    },
  },
  deleteLike: {
    type: StoryType,
    args: {
      storyId: {
        type: graphql.GraphQLString,
      },
      firebaseId: {
        type: graphql.GraphQLString,
      },
    },
    resolve: async (rootValue, args) => {
      try {
        const { storyId, firebaseId } = args;
        console.log(args);

        const story = await Stories.findByIdAndUpdate(
          { _id: storyId },
          {
            $pull: {
              likes: firebaseId,
            },
          },
          { new: true }
        );
        console.log(story);
        return story;
      } catch (ex) {
        console.log(ex);
      }
    },
  },
};
