const graphql = require('graphql');
const { ImageType, ImageInputType } = require('../types');
const { find } = require('lodash');
const { Images } = require('../../models/images.model');

module.exports = {
  insertImage: {
    type: ImageType,
    args: {
      input: {
        type: new graphql.GraphQLNonNull(ImageInputType),
      },
    },
    resolve: async (rootValue, { input }) => {
      console.log(input);
      try {
        const image = await Images.create(input);

        return image;
      } catch(ex) {
        console.log(ex);
      }
    },
  },
};
