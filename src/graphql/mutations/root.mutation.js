const graphql = require('graphql');
const { find } = require('lodash');

const RootMutation = new graphql.GraphQLObjectType({
  name: 'Mutations',
  description: 'Updates',
  fields: Object.assign(
    {},
    require('./users.mutation'),
    require('./stories.mutation'),
    require('./profiles.mutation'),
    require('./images.mutation'),   
  ),
});

module.exports = RootMutation;
