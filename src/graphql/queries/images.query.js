const graphql = require('graphql');
const { ImageType } = require('../types');
const { find, filter } = require('lodash');
const { Images } = require('../../models/images.model');

module.exports = {
  image: {
    type: ImageType,
    args: {
      id: { type: graphql.GraphQLString },
      firebaseId: { type: graphql.GraphQLString },
      guid: { type: graphql.GraphQLString },
    },
    async resolve(parentValue, args) {
      try {
        const images = await Images.find();

        if(args.firebaseId) {
          return find(images, ['firebaseId', args.firebaseId]);
        }

        if(args.guid) {
          return find(images, ['guid', args.guid]);
        }

        return find(images, ['_id', args._id]);
      }
      catch(err) {
        console.log(err);
      }
    }
  },
  images: {
    type: new graphql.GraphQLList(ImageType),
    async resolve(parentValue, args) {
      const images = await Images.find();

      return images; 
    }
  },
};