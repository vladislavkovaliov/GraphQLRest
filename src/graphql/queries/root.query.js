const graphql = require('graphql');

const RootQuery = new graphql.GraphQLObjectType({
  name: 'Query',
  fields: Object.assign(
    {},
    require('./profiles.query'),
    require('./users.query'),
    require('./stories.query'),
    require('./images.query'),    
  ),
});

module.exports = RootQuery;
