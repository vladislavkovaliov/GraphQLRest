const graphql = require('graphql');
const { StoryType } = require('../types');
const { find, filter } = require('lodash');
const { Stories } = require('../../models/stories.model');

module.exports = {
  story: {
    type: StoryType,
    args: {
      index: { type: graphql.GraphQLInt },
      id: { type: graphql.GraphQLString },
      profileId: { type: graphql.GraphQLString }
    },
    async resolve(parentValue, args) {
      try {
        const stories = await Stories.find();
        if(args.id) {
          return await Stories.findOne({ _id: args.id });
        }

        if(args.index) {
          return await Stories.findOne({ index: args.index });
        }

        if(args.profileId) {
          return await Stories.findOne({ profileId: args.profileId });
        }
      }
      catch(err) {
        console.log(err);
      }
    }
  },
  stories: {
    type: new graphql.GraphQLList(StoryType),
    args: {
      index: { type: graphql.GraphQLInt },
      id: { type: graphql.GraphQLString },
      profileId: { type: graphql.GraphQLString },
      firebaseId: { type: graphql.GraphQLString }
    },
    async resolve(parentValue, args) {
      const stories = await Stories.find();

      if(args.id) {
        return stories.filter(s => s._id.toString() === args.id);
      }

      if(args.index) {
        return stories.filter(s => s.index === args.index);
      }

      if(args.profileId) {
        return stories.filter(s => s.profileId === args.profileId);
      }

      if(args.firebaseId) {
        return stories.filter(s => s.firebaseId === args.firebaseId);
      }

      return stories;
    }
  },
};
