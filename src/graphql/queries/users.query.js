const graphql = require('graphql');
const { UserType } = require('../types');
const { find, filter } = require('lodash');
const { Users } = require('../../models/users.model');

module.exports = {
  user: {
    type: UserType,
    args: {
      index: { type: graphql.GraphQLInt }
    },
    async resolve(parentValue, args) {
      try {
        const users = await Users.find();

        return find(users, ['index', args.index]);          
      }
      catch(err) {
        console.log(err);
      }
    }
  },
  users: {
    type: new graphql.GraphQLList(UserType),
    async resolve(parentValue, args) {
      const users = await Users.find();

      return users; 
    }
  },
};
