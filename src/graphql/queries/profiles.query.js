const graphql = require('graphql');
const profiles = require('../../data/profiles.json');
const { ProfileType } = require('../types');
const { find, filter } = require('lodash');
const { Profiles } = require('../../models/profiles.model');

module.exports = {
  profile: {
    type: ProfileType,
    args: {
      id: { type: graphql.GraphQLString },
      firebaseId: { type: graphql.GraphQLString },
    },
    async resolve(parentValue, args) {
      try {
        const profiles = await Profiles.find();

        if(args.firebaseId) {
          return find(profiles, ['firebaseId', args.firebaseId]);
        }

        return find(profiles, ['id', args.id]);
      }
      catch(err) {
        console.log(err);
      }
    }
  },
  profiles: {
    type: new graphql.GraphQLList(ProfileType),
    async resolve(parentValue, args) {
      const profiles = await Profiles.find();

      return profiles; 
    }
  },
};