const firebase = require('firebase');
const jwt = require('jsonwebtoken');
const users = require('../data/users.json');
const { find } = require('lodash');
const { AUTHENTICATION_ERROR } = require('../errors/errors');
const { Profiles } = require('../models/profiles.model');

module.exports = function (overrides) {
  const base = {
    jwt: (req, res, next) => {
      const { login } = req.body;
      const user = find(users, ['login', login]);

      if (user) {
        const { password } = req.body;

        if (password && user.password === password) {
          const token = jwt.sign(user, 'secret');

          res.status(200).json({
            statusCode: 200,
            code: 200,
            success: true,
            data: {
              email: user.email,
              login: user.login,
              password: user.password,
              profileId: user.guid,
              index: user.index,
            },
            token: `JWT ${token}`,
          });
        } else {
          next(AUTHENTICATION_ERROR);
        }
      } else {
        next(AUTHENTICATION_ERROR)
      }
    },
    signIn: (req, res, next) => {
      const { email, password } = req.body;

      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(user => {
          res.status(200).json(user);
        })
        .catch(err => {
          console.log(err);
          next({ status: 500 });
        });
    },
    signUp: async (req, res, next) => {
      const { email, password } = req.body;

      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(user => {
          Profiles.create(Object.assign({}, req.body, { firebaseId: user.uid }))
            .then(profile => {
              res.status(200).json(user);
            })
            .catch(err => {
              console.log(ex);
              next({ status: 500 });
            });
        })
        .catch(err => {
          console.log(err);
          next({ status: 500 });
        });
    },
  };

  return { ...base, ...overrides};
};
