const StoriesController = require('./stories.controller');
const { Stories } = require('../models/stories.model');

const storiesCtr = StoriesController();

jest.mock('../models/stories.model', () => ({
  Stories: {
    find: jest.fn(() => {}),
    create: jest.fn(() => {}),
    findByIdAndRemove: jest.fn(() => {}),
  },
}));

const statusMock = jest.fn(() => {});
const jsonMock = jest.fn(() => {});
const res = {
  status: statusMock,
  json: jsonMock,
};

jsonMock.mockImplementation(() => res);
statusMock.mockImplementation(() => res);

let req = null;

describe('[stories.controller.js]', () => {
  beforeEach(() => {
    req = {
      params: {
        storyId: "storyId",
      },
      body: {
        text: "text",
        picture: "picture",
        title: "title"
      }, 
    };
  });

  describe('getStories()', () => {
    test('should find() be called', async () => {
      await storiesCtr.getStories(req, res);

      expect(Stories.find).toHaveBeenCalled();

      expect(res.status).toHaveBeenCalled();
      expect(res.json).toHaveBeenCalled();
    });
  });

  describe('insertStory()', () => {
    test('should create() be called', async () => {
      await storiesCtr.insertStory(req, res);

      expect(Stories.create).toHaveBeenCalled();

      expect(res.status).toHaveBeenCalled();
      expect(res.json).toHaveBeenCalled();
    });
  });

  describe('deleteById()', () => {
    test('should findByIdAndRemove() be called', async () => {
      await storiesCtr.deleteById(req, res);

      expect(Stories.findByIdAndRemove).toHaveBeenCalled();

      expect(res.status).toHaveBeenCalled();
      expect(res.json).toHaveBeenCalled();
    });
  });
});