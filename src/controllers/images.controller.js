const { Images } = require('../models/images.model');

module.exports = function (overrides) {
  const base = {
    saveImage: async (req, res, next) => {
      try {
        const image = await Images.create(req.body);

        return res.status(200).json(image);
      }
      catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
    /**
     * Should return all images by default - Array
     */
    getImages: async (req, res, next) => {
      try {
        const images = await Images.find({});

        return res.status(200).json(images);
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
  };

  return { ...base, ...overrides};
};
