const { Stories } = require('../models/stories.model');

module.exports = (overrides) => {
  const base = {
    insertStory: async(req, res, next) =>  {
      try {
        const story = await Stories.create(req.body);

        return res.status(200).json(story);
      } catch (ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
    getStories: async (req, res) => {
      const stories = await Stories.find({});

      return res.status(200).json(stories);
    },
    deleteById: async (req, res, next) => {
      const { storyId } = req.params;

      try {
        const story = await Stories.findByIdAndRemove(storyId);

        res.status(200).json(story);
      } catch(ex) {
        next({
          statusCode: 404,
          status: 404,
          msg: ex.message,
        });
      }
    },
    addLike: async (req, res, next) => {
      try {
        const { storyId, firebaseId } = req.body;

        const story = await Stories.findByIdAndUpdate(
          { _id: storyId },
          {
            $addToSet: {
              likes: firebaseId,
            },
          },
          { new: true }
        );

        res.status(200).json(story);
      } catch(ex) {
        next({
          statusCode: 404,
          status: 404,
          msg: ex.message,
        });
      }
    },
    deleteLike: async (req, res, next) => {
      try {
        const { storyId, firebaseId } = req.body;

        const story = await Stories.findByIdAndUpdate(
          { _id: storyId },
          {
            $pull: {
              likes: firebaseId,
            },
          },
          { new: true }
        );

        res.status(200).json(story);
      } catch (ex) {
        next({
          statusCode: 404,
          status: 404,
          msg: ex.message,
        });
      }
    },
  };

  return { ...base, ...overrides};
};

