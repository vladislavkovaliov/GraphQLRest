const { Stories } = require('../models/stories.model');

module.exports = function (overrides) {
  const base = {
    addLike: async (req, res, next) => {
      try {
        const { storyId, firebaseId } = req.body;

        const story = await Stories.findByIdAndUpdate(
          storyId,
          {
            $addToSet: {
              likes: firebaseId,
            },
          },
          { new: true },
        );

        return res.status(200).json(story);
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
    removeLike: async (req, res, next) => {
      try {
        const { storyId, firebaseId } = req.body;

        const story = await Stories.findByIdAndUpdate(
          storyId,
          {
            $pull: {
              likes: firebaseId,
            },
          },
          { new: true },
        );

        return res.status(200).json(story);
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
  };

  return { ...base, ...overrides};
};

