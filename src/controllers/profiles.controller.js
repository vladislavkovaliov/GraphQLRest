const { Profiles } = require('../models/profiles.model');

module.exports = function (overrides) {
  const base = {
    getProfileData: (req, res) => {
      res.status(200).json({
        getProfileData: "OK"
      });
    },
    getProfiles: async (req, res, next) => {
      try {
        const profiles = await Profiles.find({});

        return res.status(200).json(profiles);
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
    createProfile: async (req, res, next) => {
      try {
        const profile = await Profiles.create(req.body);

        return res.status(200).json(profile);
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
    deleteById: async (req, res, next) => {
      const { profileId } = req.params;

      try {
        const profile = await Profiles.findByIdAndRemove(profileId);

        res.status(200).json(profile);
      } catch(ex) {
        next({
          statusCode: 404,
          status: 404,
          msg: ex.message,
        });
      }
    },
    following: async (req, res, next) => {
      const { id: _id, profileId } = req.body;

      try {
        await Profiles
          .findOneAndUpdate(
            { _id },
            {
              $addToSet: {
                following: profileId,
              },
            },
          );

        res.status(200).json({
          following: "OK"
        });
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
    followers: async (req, res, next) => {
      const { id: _id, profileId } = req.body;

      try {
        await Profiles
          .findOneAndUpdate(
            { _id },
            {
              $addToSet: {
                followers: profileId,
              },
            },
          );

        res.status(200).json({
          followers: "OK"
        });
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
    subscribe: async (req, res, next) => {
      try {
        const { firebaseId, onFirebaseId } = req.body;
        const profile = await Profiles
          .findOneAndUpdate(
            { firebaseId: onFirebaseId },
            {
              $addToSet: {
                followers: firebaseId,
              },
            },
            { new: true }
          );

        await Profiles
          .findOneAndUpdate(
            { firebaseId },
            {
              $addToSet: {
                following: onFirebaseId,
              }
            },
            { new: true }
          );

        return res.status(200).json(profile);
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
    unsubscribe: async (req, res, next) => {
      try {
        const { firebaseId, firebaseIdToUnsubscribe } = req.body;
        const profile = await Profiles
          .findOneAndUpdate(
            { firebaseId: firebaseIdToUnsubscribe },
            {
              $pull: {
                followers: firebaseId,
              },
            },
            { new: true }
          );

        await Profiles
          .findOneAndUpdate(
            { firebaseId },
            {
              $pull: {
                following: firebaseIdToUnsubscribe,
              }
            },
            { new: true }
          );

        return res.status(200).json(profile);
      } catch(ex) {
        next({
          statusCode: 500,
          status: 500,
          msg: ex.message,
        });
      }
    },
  };

  return { ...base, ...overrides};
};

