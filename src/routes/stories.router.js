const router = require('express').Router();
const StoriesController = require('../controllers/stories.controller');

const storiesController = StoriesController();

router
  .route('/stories')
  .get(storiesController.getStories);


router
  .route('/stories')
  .post(storiesController.insertStory);

router
  .route('/stories/:storyId')
  .delete(storiesController.deleteById);

router
  .route('/stories/likes')
  .post(storiesController.addLike);

router
  .route('/stories/likes/:storyId')
  .delete(storiesController.deleteLike);

module.exports = router;
