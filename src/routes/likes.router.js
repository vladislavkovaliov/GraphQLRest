const router = require('express').Router();
const LikesController = require('../controllers/likes.controller');

const likesController = LikesController();

router
  .route('/likes')
  .post(likesController.addLike);

router
  .route('/likes')
  .delete(likesController.removeLike);


module.exports = router;
