const router = require('express').Router();
const protect = require('../middlewares/auth.middleware');
const config = require('../middlewares/config.middleware');
const ImageController = require('../controllers/images.controller');
const imageController = ImageController();

router
  .route('/images')
  .get(imageController.getImages);

router
  .route('/images')
  .post(imageController.saveImage);

module.exports = router;
