const router = require('express').Router();
const ProfilesController = require('../controllers/profiles.controller');

const profilesController = ProfilesController();

router
  .route('/profiles')
  .get(profilesController.getProfiles);

router
  .route('/profiles/:profileId')
  .delete(profilesController.deleteById);

router
  .route('/profiles')
  .post(profilesController.createProfile);

router
  .route('/profiles/following')
  .post(profilesController.following);

router
  .route('/profiles/followers')
  .post(profilesController.followers);

router
  .route('/profiles/subscribe')
  .post(profilesController.subscribe);

router
  .route('/profiles/unsubscribe')
  .post(profilesController.unsubscribe);

module.exports = router;
