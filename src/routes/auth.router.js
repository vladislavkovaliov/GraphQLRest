const router = require('express').Router();
const AuthController = require('../controllers/auth.controller');

const authController = AuthController();

router
  .route('/')
  .post(authController.jwt);

router
  .route('/signIn')
  .post(authController.signIn);

router
  .route('/signUp')
  .post(authController.signUp);

module.exports = router;
