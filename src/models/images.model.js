const { schemaGenerator } = require('../utils/schema-generator.util');
const { modelGenerator } = require('../utils/model-generator.util');
const R = require('ramda');

const Images = R.compose(
  R.curry(modelGenerator)('images'),
  schemaGenerator,
); 

module.exports.Images = Images({
  url: String,
  firebaseId: String,
  guid: String,
  profileId: String,
});
