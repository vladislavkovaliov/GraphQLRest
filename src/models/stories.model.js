const { schemaGenerator } = require('../utils/schema-generator.util');
const { modelGenerator } = require('../utils/model-generator.util');
const R = require('ramda');

const Stories = R.compose(
  R.curry(modelGenerator)('stories'),
  schemaGenerator,
);

module.exports.Stories = Stories({
  index: Number,
  guid: String,
  picture: String,
  date: String,
  title: String,
  text: String,
  likes: [],
  comments: [],
  profileId: String,
  firebaseId: String,
});
